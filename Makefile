
tests:
	docker pull php
	@docker run --rm \
	-v $(CURDIR):/data \
	-w /data \
	php /data/vendor/bin/phpunit \
	--bootstrap /data/vendor/autoload.php \
	/data/Tests

composer:
	@docker run --rm \
		-v $(CURDIR):/data \
		imega/composer \
		update --ignore-platform-reqs
