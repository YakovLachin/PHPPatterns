<?php

namespace PHPPatterns\Creational\AbstractFactory;

class Writer
{
    public function write(AbstractObject $text)
    {
        echo $text->getValue();
    }
}
