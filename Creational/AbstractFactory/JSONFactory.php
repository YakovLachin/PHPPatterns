<?php

namespace PHPPatterns\Creational\AbstractFactory;

class JSONFactory extends AbstractFactory
{
    protected $model;

    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * @return AbstractObject
     */
    public function createObject()
    {
        $obj = new JSONObject();
        $obj->setValue(json_encode((array)$this->model));

        return $obj;
    }
}
