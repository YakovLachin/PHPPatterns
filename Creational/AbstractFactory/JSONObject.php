<?php

namespace PHPPatterns\Creational\AbstractFactory;

class JSONObject extends AbstractObject
{
    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }
}
