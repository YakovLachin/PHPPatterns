<?php

namespace PHPPatterns\Creational\AbstractFactory;

abstract class AbstractObject
{
    /**
     * @var mixed
     */
    protected $value;

    /**
     * @return mixed
     */
    abstract public function getValue();
}
