<?php

namespace PHPPatterns\Creational\AbstractFactory;

class XMLFactory extends AbstractFactory
{

    protected $model;

    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * @return AbstractObject
     */
    public function createObject()
    {
        $obj = new XMLObject();

        $xml = new \SimpleXMLElement('<?xml version="1.0"?><data></data>');
        $this->arrayToXml((array)$this->model, $xml);
        $obj->setValue($xml->asXML());

        return $obj;
    }

    /**
     * @param mixed[] $array
     * @param \SimpleXMLElement $xml
     */
    function arrayToXml($array, &$xml)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                //если элемент число, то добавляем префикс, с цифр не может начинаться
                if (!is_numeric($key)) {
                    $subnode = $xml->addChild("$key");
                    $this->arrayToXml($value, $subnode);
                } else {
                    $subnode = $xml->addChild("item$key");
                    $this->arrayToXml($value, $subnode);
                }
            } else {
                if (!is_numeric($key)) {
                    $xml->addChild("$key", "$value");
                } else {
                    $xml->addChild("item$key", "$value");
                }
            }
        }
    }
}
