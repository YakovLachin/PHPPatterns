<?php

namespace PHPPatterns\Creational\AbstractFactory;

abstract class AbstractFactory
{
    /**
     * @return AbstractObject
     */
    abstract public function createObject();
}
