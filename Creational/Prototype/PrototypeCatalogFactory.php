<?php

namespace PHPPatterns\Creational\Prototype;

class PrototypeCatalogFactory
{
    /**
     * @var Catalog
     */
    private $catalog;

    /**
     * PrototypeCategoryFactory constructor.
     * @param Catalog $catalog
     */
    public function __construct(Catalog $catalog)
    {
        $this->catalog = $catalog;
    }


    public function getCatalog()
    {
        return $this->catalog->cloneSelf();
    }
}