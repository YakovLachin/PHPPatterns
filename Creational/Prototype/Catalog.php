<?php

namespace PHPPatterns\Creational\Prototype;

class Catalog
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Product[] $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @var string
     */
    protected $name;

    /**
     * @var Product[]
     */
    protected $products;

    function cloneSelf()
    {
        $res       = new Catalog();
        $res->name = $this->name;
        $res->id   = 0;
        $products  = array();
        foreach ($this->products as $product) {
            array_push($products, $product->cloneSelf());
        }

        return $res;
    }
}