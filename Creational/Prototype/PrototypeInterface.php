<?php

namespace PHPPatterns\Creational\Prototype;

interface PrototypeInterface
{
    public function cloneSelf();
}