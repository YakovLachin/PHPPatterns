<?php

namespace PHPPatterns\Creational\Prototype;

class PrototypeProductFactory
{
    /**
     * @var Product
     */
    private $product;

    /**
     * PrototypeProductFactory constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product->cloneSelf();
    }
}