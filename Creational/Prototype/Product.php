<?php


namespace PHPPatterns\Creational\Prototype;


class Product implements PrototypeInterface
{
    /**
     * @var string
     */
    protected $scu;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return string
     */
    public function getScu()
    {
        return $this->scu;
    }

    /**
     * @param string $scu
     */
    public function setScu($scu)
    {
        $this->scu = $scu;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @var string
     */
    protected $price;

    public function cloneSelf()
    {
        $res         = new Product();
        $res->name   = $this->name;
        $res->scu    = '';
        $res->price  = $this->price;

        return $res;
    }
}