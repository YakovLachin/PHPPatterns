<?php

namespace PHPPatterns\Creational\Builder;

interface BuilderInterface
{
    public function setPrice();

    public function buildBox();

    public function setMarkup();

    public function createProduct();
}
