<?php

namespace PHPPatterns\Creational\Builder;

class Director
{
    /**
     * @var BuilderInterface
     */
    private $builder;

    /**
     * Director constructor.
     *
     * @param BuilderInterface $builder
     */
    public function __construct(BuilderInterface &$builder)
    {
        $this->builder = $builder;
    }

    public function construct() {
        $this->builder->createProduct();
        $this->builder->setPrice();
        $this->builder->buildBox();
        $this->builder->setMarkup();
    }
}
