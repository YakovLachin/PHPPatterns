<?php

namespace PHPPatterns\Creational\Builder;

class ExpensiveBuilder implements BuilderInterface
{
    /**
     * @var Product
     */
    private $product;

    public function getResult()
    {
        return $this->product;
    }

    public function setPrice()
    {
        $this->product->price = 100;
    }

    public function buildBox()
    {
        $box                = new Box();
        $box->type          = Box::GOOD;
        $this->product->box = $box;
    }

    public function setMarkup()
    {
        $this->product->markup = 200.00;
    }

    public function createProduct()
    {
        $this->product = new Product();
    }
}
