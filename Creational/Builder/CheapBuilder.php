<?php

namespace PHPPatterns\Creational\Builder;

class CheapBuilder implements BuilderInterface
{
    /**
     * @var Product
     */
    private $product;

    public function createProduct()
    {
        $this->product = new Product();
    }

    public function getResult()
    {
        return $this->product;
    }

    public function setPrice()
    {
        $this->product->price = 100;
    }

    public function buildBox()
    {
        $box                = new Box();
        $box->type          = Box::BAD;
        $this->product->box = $box;
    }

    public function setMarkup()
    {
        $this->product->markup = 10;
    }
}
