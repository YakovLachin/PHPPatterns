<?php

namespace PHPPatterns\Creational\Builder;

class Product
{
    /**
     * @var Box
     */
    public $box;

    /**
     * @var float
     */
    public $markup;

    /**
     * @var float
     */
    public $price;
}
