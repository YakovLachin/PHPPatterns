<?php

namespace PHPPatterns\Creational\Builder;

class Box
{
    const BAD = "none";

    const GOOD = "metal-box";

    /**
     * @var string
     */
    public $type;
}
