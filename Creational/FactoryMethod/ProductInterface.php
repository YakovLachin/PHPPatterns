<?php

namespace PHPPatterns\Creational\FactoryMethod;

interface ProductInterface
{
    public function setPrice($price);
    public function getPrice();
    public function getMarkup();
    public function setMarkup($markup);
    public function getTotal();
}
