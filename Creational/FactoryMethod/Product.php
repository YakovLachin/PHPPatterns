<?php

namespace PHPPatterns\Creational\FactoryMethod;

class Product implements ProductInterface
{
    /**
     * @var float
     */
    protected $price;

    /**
     * @var float
     */
    protected $markup;

    /**
     * @param float $price
     */
    function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    function getPrice()
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function getMarkup()
    {
        return $this->markup;
    }

    /**
     * @param float $markup
     */
    public function setMarkup($markup)
    {
        $this->markup = $markup;
    }

    public function getTotal() {
        return $this->price + $this->markup;
    }
}
