<?php

namespace PHPPatterns\Creational\FactoryMethod;

interface ProductFactoryInterface
{
    public function createProduct();
    public function makeProduct();
}
