<?php

namespace PHPPatterns\Creational\FactoryMethod;

class ProductFactory implements  ProductFactoryInterface
{
    const CURRENT_MARKUP = 250.00;

    /**
     * This is factory Method!!!
     *
     * @return ProductInterface
     */
    public function createProduct() {
        return new Product();
    }

    public function makeProduct() {
        $product = $this->createProduct();
        $product->setMarkup(self::CURRENT_MARKUP);

        return $product;
    }
}
