<?php

namespace PHPPatterns\Creational\FactoryMethod;

class ExpensiveFactory extends ProductFactory
{
    public function createProduct()
    {
        return new ExpensiveProduct();
    }
}
