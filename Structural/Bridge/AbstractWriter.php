<?php

namespace PHPPatterns\Structural\Bridge;

abstract class AbstractWriter
{
    /**
     * @var FormatterInterface
     */
    protected $formatter;

    /**
     * AbstractHandler constructor.
     * @param FormatterInterface $formatter
     */
    public function __construct(FormatterInterface $formatter)
    {
        $this->formatter = $formatter;
    }

    /**
     * @param FormatterInterface $formatter
     */
    public function setFormatter($formatter)
    {
        $this->formatter = $formatter;
    }

    public abstract function handle();
}
