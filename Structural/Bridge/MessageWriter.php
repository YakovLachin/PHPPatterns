<?php

namespace PHPPatterns\Structural\Bridge;

class MessageWriter extends AbstractWriter
{
    public function handle()
    {
        $text = "We need more minerals!!!";

        return $this->formatter->format($text);
    }
}