<?php

namespace PHPPatterns\Structural\Bridge;

class HtmlFormatter implements FormatterInterface
{

    public function format($text)
    {
        $tag = "h2";
        return "<" . $tag . ">" . $text . "</" . $tag . ">";
    }
}