<?php

namespace PHPPatterns\Structural\Bridge;

interface FormatterInterface
{
    public function format($text);
}