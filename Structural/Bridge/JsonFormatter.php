<?php

namespace PHPPatterns\Structural\Bridge;

class JsonFormatter implements  FormatterInterface
{
    public function format($text)
    {
        $res["message"] = $text;

        return json_encode($res);
    }
}