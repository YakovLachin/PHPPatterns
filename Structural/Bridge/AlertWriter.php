<?php

namespace PHPPatterns\Structural\Bridge;

class AlertWriter extends AbstractWriter
{
    public function handle()
    {
        $text = "We are under attack!!!";

        return $this->formatter->format($text);
    }
}