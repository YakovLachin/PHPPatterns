<?php

namespace PHPPatterns\Structural\Flyweight;

class ConcreteFlyweight implements FlyweightInterface
{

    protected $intrinsicState;

    /**
     * ConcreteFlyweight constructor.
     */
    public function __construct($char)
    {
        $this->intrinsicState = $char;
    }

    public function operation($extrinsicState)
    {
        return "$extrinsicState" . $this->intrinsicState;
    }
}
