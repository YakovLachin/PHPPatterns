<?php

namespace PHPPatterns\Structural\Flyweight;

interface FlyweightInterface
{
    public function operation($extrinsicState);
}