<?php

namespace PHPPatterns\Structural\Flyweight;

class FlyweightFactory
{
    protected $chars = array();

    public function createFlyweight($key) {
        if (!isset($this->chars[$key])) {
            $this->chars[$key] = new ConcreteFlyweight($key);
        }

        return $this->chars[$key];
    }

    public function createRowFlyweight($string)
    {
        $chars = str_split($string);
        $row = new RowFlyweight();
        foreach ($chars as $char) {
            $row->Insert($this->createFlyweight($char));
        }

        return $row;
    }

    /**
     * @return int
     */
    public function count() {
         return count($this->chars);
    }
}
