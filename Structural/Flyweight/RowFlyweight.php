<?php

namespace PHPPatterns\Structural\Flyweight;

class RowFlyweight implements FlyweightInterface
{
    /**
     * @var ConcreteFlyweight[]
     */
    protected $chars;

    /**
     * @var int
     */
    protected $index;

    /**
     * @var array
     */
    protected $fonts;

    /**
     * RowFlyweight constructor.
     */
    public function __construct()
    {
        $this->index = 0;
    }

    public function operation($extrinsicState)
    {
        $res = "";
        foreach ($this->chars as $char) {
            $res = $res . $char->operation($extrinsicState);
        }

        return $res;
    }

    public function Insert(ConcreteFlyweight $char)
    {
        $this->chars[] = $char;
        $this->index++;
    }

    /**
     * @return mixed
     */
    public function getFonts()
    {
        return $this->fonts;
    }

    /**
     * @param mixed $fonts
     */
    public function setFonts($fonts)
    {
        $this->fonts = $fonts;
    }
}
