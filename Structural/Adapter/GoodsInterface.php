<?php

namespace PHPPatterns\Structural\Adapter;

interface GoodsInterface
{
    public function getPrice();
}