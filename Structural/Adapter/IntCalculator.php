<?php

namespace PHPPatterns\Structural\Adapter;

class IntCalculator
{
    /**
     * @return int
     */
    public function calculateSumm($a, $b)
    {
        return $a + $b;
    }
}