<?php

namespace PHPPatterns\Structural\Adapter;

class Client
{
    /**
     * @var ExecutorInterface
     */
    private $summator;

    const MARKUP = 100;

    /**
     * Client constructor.
     * @param ExecutorInterface $executor
     */
    public function __construct(ExecutorInterface $executor)
    {
        $this->summator = $executor;
    }


    /**
     * Вернуть стоимость.
     *
     * @return int
     */
    public function getPrice(GoodsInterface $product)
    {
        $res = $this->summator->invoke(self::MARKUP, $product->getPrice());

        return $res;
    }
}