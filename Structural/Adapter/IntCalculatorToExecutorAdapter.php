<?php

namespace PHPPatterns\Structural\Adapter;

class IntCalculatorToExecutorAdapter implements ExecutorInterface
{
    /**
     * @var
     */
    private $intCalculator;

    /**
     * IntCalculatorToExecutorAdapter constructor.
     *
     * @param $intCalculator
     */
    public function __construct(IntCalculator $intCalculator)
    {
        $this->intCalculator = $intCalculator;
    }

    /**
     * @param []mixed $data
     * @return mixed
     */
    public function invoke(...$data)
    {
        $summ = $this->intCalculator->calculateSumm($data[0], $data[1]);

        return $summ;
    }
}