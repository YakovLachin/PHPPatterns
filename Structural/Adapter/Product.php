<?php

namespace PHPPatterns\Structural\Adapter;

/**
 * Class Product
 * @package PHPPatterns\Strctural\Adapter
 */
class Product implements GoodsInterface
{

    /**
     * @var int
     */
    protected $price;

    /**
     * @param int $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }
}