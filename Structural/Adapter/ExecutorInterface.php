<?php

namespace PHPPatterns\Structural\Adapter;

interface ExecutorInterface
{
    /**
     * @param []int $data
     * @return mixed
     */
    public function invoke(...$data);
}