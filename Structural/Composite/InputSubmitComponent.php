<?php

namespace PHPPatterns\Structural\Composite;

class InputSubmitComponent implements ComponentInterface
{
    public function content()
    {
        return "<div><input type='submit'></div>";
    }
}