<?php

namespace PHPPatterns\Structural\Composite;

interface CompositeInterface
{
    /**
     * @param  int $index
     * @return ComponentInterface
     */
    public function getChild($index);

    /**
     * @param ComponentInterface $node
     */
    public function remove(ComponentInterface $node);

    /**
     * @param ComponentInterface $node
     */
    public function add(ComponentInterface $node);
}