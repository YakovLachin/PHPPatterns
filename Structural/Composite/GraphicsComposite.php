<?php

namespace PHPPatterns\Structural\Composite;

class GraphicsComposite implements CompositeInterface, ComponentInterface
{
    /**
     * @var ComponentInterface[]
     */
    protected $childs;

    /**
     * @param  int $index
     * @return ComponentInterface
     */
    public function getChild($index)
    {
        if (array_key_exists($index, $this->childs)) {
            return $this->childs[$index];
        }
    }

    /**
     * @param ComponentInterface $node
     */
    public function remove(ComponentInterface $node)
    {
       $key = array_search($node, $this->childs);
       if (false !== $key) {
           unset($this->childs[$key]);
       }
    }

    /**
     * @param ComponentInterface $node
     *
     * @return ComponentInterface
     */
    public function add(ComponentInterface $node)
    {
        $this->childs[] = $node;

        return $node;
    }

    public function content()
    {
        $result = "";
        foreach ($this->childs as $child) {
            $result = $result . $child->content();
        }

        return $result;
    }
}