<?php

namespace PHPPatterns\Structural\Composite;

class InputTextComponent implements ComponentInterface
{
    public function content()
    {
        return "<div><input type='text'></div>";
    }
}