<?php

namespace PHPPatterns\Structural\Composite;

class FormBlock extends GraphicsComposite
{
    public function content()
    {
        $content = "";
        /**
         * @var ComponentInterface $child
         */
        foreach ($this->childs as $child) {
            $content = $content . $child->content();
        }

        return "<form>" . $content . "</form>";
    }
}