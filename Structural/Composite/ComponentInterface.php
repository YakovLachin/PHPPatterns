<?php

namespace PHPPatterns\Structural\Composite;

interface ComponentInterface
{
    /**
     * @return string
     */
    public function content();
}