<?php

namespace PHPPatterns\Structural\Composite;

class FormComposite extends GraphicsComposite
{
    public function content()
    {
        $result = "<form>";
        foreach ($this->childs as $child) {
            $result = $result . $child->content();
        }
        return $result . "</form>";
    }
}