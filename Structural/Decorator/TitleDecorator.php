<?php

namespace PHPPatterns\Structural\Decorator;

class TitleDecorator implements DrawInterface
{
    /**
     * @var DrawInterface
     */
    private $draw;

    /**
     * TitleDecorator constructor.
     * @param DrawInterface $draw
     */
    public function __construct(DrawInterface $draw)
    {
        $this->draw = $draw;
    }

    public function draw()
    {
        return "<h2>" . $this->draw->draw() . "</h2>";
    }
}