<?php

namespace PHPPatterns\Structural\Decorator;

class BorderDecorator implements DrawInterface
{
    /**
     * @var DrawInterface
     */
    private $element;


    /**
     * BorderDecorator constructor.
     * @param DrawInterface $element
     */
    public function __construct(DrawInterface $element)
    {
        $this->element = $element;
    }

    public function draw()
    {
        $result = "<div style='border: solid'>";
        $result = $result . $this->element->draw();
        $result = $result . "</div>";

        return $result;
    }
}