<?php

namespace PHPPatterns\Structural\Decorator;

interface DrawInterface
{
    public function draw();
}