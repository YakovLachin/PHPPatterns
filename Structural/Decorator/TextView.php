<?php

namespace PHPPatterns\Structural\Decorator;

class TextView implements DrawInterface
{
    public function draw()
    {
        return "<div>Text element</div>";
    }
}