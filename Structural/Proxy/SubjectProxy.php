<?php

namespace PHPPatterns\Structural\Proxy;

class SubjectProxy implements SubjectInterface
{

    /**
     * @var SubjectInterface
     */
    protected $original;

    /**
     * @var string
     */
    protected $state;

    /**
     * Difficult operation proxies to Original Subject
     */
    public function draw()
    {
        if (!isset($original)) {
            $this->original = new Subject();
        }

        return $this->original->draw();
    }

    /**
     * Light operation invokes in current class.
     *
     * @param $state
     */
    public function setState($state)
    {
        if (!isset($this->original)) {
            $this->state = $state;
        } else {
            $this->original->setState($state);
        }
    }
}