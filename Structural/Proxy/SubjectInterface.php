<?php

namespace PHPPatterns\Structural\Proxy;

interface SubjectInterface
{
    public function draw();
    public function setState($state);
}