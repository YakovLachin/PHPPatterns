<?php

namespace PHPPatterns\Structural\Proxy;

class Subject implements SubjectInterface
{

    /**
     * @var string
     */
    protected $state;

    public function draw()
    {
        return $this->state . "Some difficult operation of draw";
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }
}