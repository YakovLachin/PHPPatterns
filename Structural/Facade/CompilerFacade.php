<?php

namespace PHPPatterns\Structural\Facade;

class CompilerFacade
{
    /**
     * @var Scanner
     */
    private $scaner;
    /**
     * @var Parser
     */
    private $parser;
    /**
     * @var Builder
     */
    private $builder;
    /**
     * @var CodeGenerator
     */
    private $generator;

    /**
     * NodeCreatorFacade constructor.
     * @param Scanner $scanner
     * @param Parser $parser
     * @param CodeGenerator $generator
     */
    public function __construct(
        Scanner $scanner,
        Parser $parser,
        CodeGenerator $generator
    ) {

        $this->scaner    = $scanner;
        $this->parser    = $parser;
        $this->generator = $generator;
    }

    public function compile($stdin) {
        $this->scaner->scan($stdin);
        $tokens = $this->parser->parse($this->scaner, $this->builder);
        $this->generator->generate($tokens);
    }
}