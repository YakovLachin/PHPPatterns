<?php

namespace PHPPatterns\Behavior\Interpreter;

class EqualExpression extends AlterExpression
{
    protected function doInterpret(Context $ctx, $fst, $sec)
    {
        $ctx->replace($this, $fst === $sec);
    }
}