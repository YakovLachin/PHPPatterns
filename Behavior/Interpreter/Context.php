<?php

namespace PHPPatterns\Behavior\Interpreter;

class Context
{
    /**
     * @var array
     */
    private $exprStore;

    public function replace(Expression $expr, $val)
    {
        $this->exprStore[$expr->getKey()] = $val;
    }

    public function lookup(Expression $expr)
    {
        return $this->exprStore[$expr->getKey()];
    }
}
