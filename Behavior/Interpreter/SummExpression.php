<?php

namespace PHPPatterns\Behavior\Interpreter;

class SummExpression extends OperatorExpression
{
    protected function doInterpret(Context $ctx, $fstOperand, $secOperand)
    {
        $ctx->replace($this, $fstOperand + $secOperand);
    }
}
