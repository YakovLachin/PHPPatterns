<?php

namespace PHPPatterns\Behavior\Interpreter;

abstract class AlterExpression extends Expression
{
    /**
     * @var Expression
     */
    protected $fstExpr;

    /**
     * @var Expression
     */
    protected $secExpr;

    /**
     * BoolAnExpression constructor.
     *
     * @param Expression $fstExpr
     * @param Expression $secExpr
     */
    public function __construct(Expression $fstExpr, Expression  $secExpr)
    {
        $this->fstExpr = $fstExpr;
        $this->secExpr = $secExpr;
    }

    public function interpret(Context $ctx)
    {
        $this->fstExpr->interpret($ctx);
        $this->secExpr->interpret($ctx);
        $fstVal = $ctx->lookup($this->fstExpr);
        $secVal = $ctx->lookup($this->secExpr);

        $this->doInterpret($ctx, $fstVal, $secVal);
    }

    protected abstract function doInterpret(Context $ctx, $fst, $sec);
}
