<?php

namespace PHPPatterns\Behavior\Interpreter;

abstract class OperatorExpression extends Expression
{
    /**
     * @var Expression
     */
    protected $leftOperand;

    /**
     * @var Expression
     */
    protected $rightOperand;

    /**
     * OperatorExpression constructor.
     *
     * @param Expression $leftOperand
     * @param Expression $rightOperand
     */
    public function __construct(Expression $leftOperand, Expression $rightOperand)
    {
        $this->leftOperand = $leftOperand;
        $this->rightOperand = $rightOperand;
    }

    public function interpret(Context $ctx)
    {
        $this->leftOperand->interpret($ctx);
        $this->rightOperand->interpret($ctx);
        $leftVal = $ctx->lookup($this->leftOperand);
        $rightVal = $ctx->lookup($this->rightOperand);

        $this->doInterpret($ctx, $leftVal, $rightVal);
    }

    protected abstract function doInterpret(Context $ctx, $fstOperand, $secOperand);
}
