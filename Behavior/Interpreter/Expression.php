<?php

namespace PHPPatterns\Behavior\Interpreter;

abstract class Expression
{
    abstract public function interpret(Context $ctx);

    /**
     * @var int
     */
    private $key;

    /**
     * @var int
     */
    private static $keyCount = 0;

    public function getKey()
    {
        if (! isset($this->key)) {
            self::$keyCount++;
            $this->key = self::$keyCount;
        }

        return $this->key;
    }
}
