<?php

namespace PHPPatterns\Behavior\Interpreter;

class BoolAndExpression extends AlterExpression
{
    protected function doInterpret(Context $ctx, $fst, $sec)
    {
        $ctx->replace($this, $fst && $sec);
    }
}
