<?php

namespace PHPPatterns\Behavior\Interpreter;

class LiteralExpression extends Expression
{
    protected $literal;

    /**
     * LiteralExpression constructor.
     * @param $literal
     */
    public function __construct($literal)
    {
        $this->literal = $literal;
    }

    public function interpret(Context $ctx)
    {
        $ctx->replace($this, $this->literal);
    }
}
