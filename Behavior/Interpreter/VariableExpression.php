<?php

namespace PHPPatterns\Behavior\Interpreter;

class VariableExpression extends Expression
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $val;

    /**
     * VariableExpression constructor.
     *
     * @param string $name
     * @param string $val
     */
    public function __construct($name, $val = null)
    {
        $this->name = $name;
        $this->val = $val;
    }

    public function interpret(Context $ctx)
    {
        if (null !== $this->val) {
            $ctx->replace($this, $this->val);
        }
    }

    /**
     * @param string $val
     */
    public function setVal($val)
    {
        $this->val = $val;
    }

    public function getKey()
    {
        return $this->name;
    }
}
