<?php

namespace PHPPatterns\Behavior\Memento;

class Memento
{
    /**
     * @var int
     */
    private $state;

    /**
     * Memento constructor.
     * @param int $state
     */
    public function __construct($state)
    {
        $this->state = $state;
    }


    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param int $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }
}