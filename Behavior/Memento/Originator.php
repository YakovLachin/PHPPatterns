<?php

namespace PHPPatterns\Behavior\Memento;

class Originator
{
    /**
     * @var int
     */
    private $state = 0;

    public function createMemento()
    {
        return new Memento($this->state);
    }

    /**
     * @param Memento $memento
     */
    public function setMemento($memento)
    {
        $this->state = $memento->getState();
    }

    public function increment()
    {
        $this->state++;
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }
}