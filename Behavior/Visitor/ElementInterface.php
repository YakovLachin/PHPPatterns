<?php

namespace PHPPatterns\Behavior\Visitor;

interface ElementInterface
{
    public function accept(VisitorInterface $visitor);
}