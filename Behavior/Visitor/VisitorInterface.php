<?php

namespace PHPPatterns\Behavior\Visitor;

interface VisitorInterface
{
    public function visit(ElementInterface $accept);
}