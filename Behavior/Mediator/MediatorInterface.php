<?php

namespace PHPPatterns\Behavior\Mediator;

interface MediatorInterface
{
    public function changed(WidgetInterface $widget);
}