<?php

namespace PHPPatterns\Behavior\Mediator;

class Input implements WidgetInterface
{
    /**
     * @var
     */
    private $statement;

    /**
     * @var MediatorInterface
     */
    private $mediator;

    /**
     * Button constructor.
     * @param MediatorInterface $mediator
     */
    public function __construct(MediatorInterface $mediator)
    {
        $this->statement = false;
        $this->mediator  = $mediator;
    }

    /**
     * @return bool
     */
    public function getStatement()
    {
        return $this->statement;
    }

    /**
     * @param bool $statement
     */
    public function setStatement($statement)
    {
        $this->statement = $statement;
        $this->mediator->changed($this);
    }

}