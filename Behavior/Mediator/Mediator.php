<?php

namespace PHPPatterns\Behavior\Mediator;

class Mediator implements MediatorInterface
{
    /**
     * @var Button
     */
    private $submitButton;

    /**
     * @return Button
     */
    public function getSubmitButton()
    {
        return $this->submitButton;
    }

    /**
     * @param Button $submitButton
     */
    public function setSubmitButton($submitButton)
    {
        $this->submitButton = $submitButton;
    }

    /**
     * @return Input
     */
    public function getFormInput()
    {
        return $this->formInput;
    }

    /**
     * @param Input $formInput
     */
    public function setFormInput($formInput)
    {
        $this->formInput = $formInput;
    }

    /**
     * @return ResultOutput
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * @param ResultOutput $output
     */
    public function setOutput($output)
    {
        $this->output = $output;
    }

    /**
     * @var Input
     */
    private $formInput;

    /**
     * @var ResultOutput
     */
    private $output;

    public function changed(WidgetInterface $widget)
    {
        switch (true) {
           case $widget instanceof Button:
                   $this->submitForm();
               break;
           case $widget instanceof Input:
               if ($this->submitButton->getStatement() == false) {
                   $this->displayInputForm($widget);
               }
               break;
        }
    }

    private function submitForm()
    {
        $this->output->setOutput("Form is submitted");
    }

    public function displayInputForm(Input $input)
    {
        $this->output->setOutput("Input is " . $input->getStatement());
    }
}
