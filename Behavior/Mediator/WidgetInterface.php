<?php

namespace PHPPatterns\Behavior\Mediator;

interface WidgetInterface
{
    /**
     * @return bool
     */
    public function getStatement();
}