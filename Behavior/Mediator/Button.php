<?php

namespace PHPPatterns\Behavior\Mediator;

class Button implements WidgetInterface
{
    /**
     * @var
     */
    private $statement;

    /**
     * @var MediatorInterface
     */
    private $mediator;

    /**
     * Button constructor.
     * @param MediatorInterface $mediator
     */
    public function __construct(MediatorInterface $mediator)
    {
        $this->statement = false;
        $this->mediator  = $mediator;
    }


    /**
     * @return bool
     */
    public function getStatement()
    {
        return $this->statement;
    }

    public function onclick()
    {
        $this->statement = !$this->statement;
        $this->mediator->changed($this);
    }
}