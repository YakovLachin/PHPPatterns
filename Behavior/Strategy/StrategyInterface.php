<?php

namespace PHPPatterns\Behavior\Strategy;

interface StrategyInterface
{
    public function operation();
}