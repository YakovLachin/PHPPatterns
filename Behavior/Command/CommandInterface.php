<?php

namespace PHPPatterns\Behavior\Command;

interface CommandInterface
{
    /**
     * @return mixed
     */
    public function execute();
}