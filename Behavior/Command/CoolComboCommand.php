<?php

namespace PHPPatterns\Behavior\Command;

class CoolComboCommand implements CommandInterface
{
    /**
     * @var Receiver
     */
    private $receiver;

    /**
     * AnotherCommand constructor.
     * @param Receiver $receiver
     */
    public function __construct(Receiver $receiver)
    {
        $this->receiver = $receiver;
    }

    /**
     * @return mixed
     */
    public function execute()
    {
        $res = $this->receiver->anotherAction() . " and " . $this->receiver->action();
        for ($i = 0; $i < 3; $i++) {
            $res = $res . " and ". $this->receiver->anotherAction();
        }

        return $res;
    }
}