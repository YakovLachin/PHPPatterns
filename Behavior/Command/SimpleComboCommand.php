<?php

namespace PHPPatterns\Behavior\Command;

class SimpleComboCommand implements CommandInterface
{
    /**
     * @var Receiver
     */
    private $receiver;

    /**
     * SimpleCommand constructor.
     * @param Receiver $receiver
     */
    public function __construct(Receiver $receiver)
    {
        $this->receiver = $receiver;
    }

    /**
     * @return mixed
     */
    public function execute()
    {
        return $this->receiver->action() . " and " . $this->receiver->anotherAction();
    }
}