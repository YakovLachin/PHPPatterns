<?php

namespace PHPPatterns\Behavior\Command;

class Invoker
{
    /**
     * @var CommandInterface
     */
    private $command;

    /**
     * Invoker constructor.
     * @param CommandInterface $command
     */
    public function __construct(CommandInterface $command)
    {
        $this->command = $command;
    }


    /**
     * @return mixed
     */
    public function run()
    {
        return $this->command->execute();
    }
}