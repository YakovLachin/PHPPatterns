<?php

namespace PHPPatterns\Behavior\Command;

class Receiver
{

    public function action()
    {
        return "hook";
    }

    public function anotherAction()
    {
        return "lower kick";
    }
}