<?php

namespace PHPPatterns\Behavior\State;

class IsNotActiveState implements StateInterface
{
    /**
     * @var ResultOutput
     */
    private $out;

    /**
     * @var ContextInterface
     */
    protected $context;

    /**
     * IsActiveState constructor.
     * @param ResultOutput $out
     */
    public function __construct(ResultOutput $out, ContextInterface $context)
    {
        $this->out     = $out;
        $this->context = $context;
    }

    public function operation()
    {
        $this->out->setOutput("Is active");
        $this->context->setState(new IsActiveState($this->out, $this->context));
    }
}