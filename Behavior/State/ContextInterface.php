<?php

namespace PHPPatterns\Behavior\State;

interface ContextInterface
{
    public function setState(StateInterface $state);
}