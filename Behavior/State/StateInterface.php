<?php

namespace PHPPatterns\Behavior\State;

interface StateInterface
{
    public function operation();
}