<?php

namespace PHPPatterns\Behavior\State;

class Context implements ContextInterface, StateInterface
{
    /**
     * @var StateInterface
     */
    protected $state;

    /**
     * Context constructor.
     */
    public function __construct()
    {

    }

    public function setState(StateInterface $state)
    {
        $this->state = $state;
    }

    public function operation()
    {
        $this->state->operation($this);
    }
}