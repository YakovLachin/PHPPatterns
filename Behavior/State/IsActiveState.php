<?php

namespace PHPPatterns\Behavior\State;

class IsActiveState implements StateInterface
{
    /**
     * @var ContextInterface
     */
    protected $context;
    /**
     * @var ResultOutput
     */

    private $out;

    /**
     * IsActiveState constructor.
     * @param ResultOutput $out
     * @param ContextInterface $context
     */
    public function __construct(ResultOutput $out, ContextInterface $context)
    {
        $this->out     = $out;
        $this->context = $context;
    }

    public function operation()
    {
        $this->out->setOutput("Is not active");
        $this->context->setState(new IsNotActiveState($this->out, $this->context));
    }
}