<?php

namespace PHPPatterns\Behavior\TemplateMethod;

class ProductWithDiscount extends AbstractProduct
{
    public function doRecountTotal()
    {
        $this->total = $this->price - $this->discount + $this->shippingPrice;
    }

}