<?php

namespace PHPPatterns\Behavior\TemplateMethod;

class ProductWithoutDiscount extends AbstractProduct
{
    public function doRecountTotal()
    {
        $this->total = $this->price + $this->shippingPrice;
    }
}