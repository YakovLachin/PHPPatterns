<?php

namespace PHPPatterns\Behavior\TemplateMethod;

abstract class AbstractProduct
{
    /**
     * @var float
     */
    protected $price;

    /**
     * @var float
     */
    protected $discount;

    /**
     * @var float
     */
    protected $shippingPrice;

    /**
     * @var float
     */
    protected $total;

    abstract public function doRecountTotal();

    /**
     * @return mixed
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @param mixed $discount
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }
}
