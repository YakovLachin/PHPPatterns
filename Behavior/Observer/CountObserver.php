<?php

namespace PHPPatterns\Behavior\Observer;

class CountObserver implements ObserverInterface
{
    /**
     * @var int
     */
    private $count = 0;

    /**
     * @void
     */
    public function update()
    {
        $this->count++;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }
}