<?php

namespace PHPPatterns\Behavior\Observer;

interface ObserverInterface
{
    /**
     * @void
     */
    public function update();
}