<?php

namespace PHPPatterns\Behavior\Observer;

class Subject implements SubjectInterface
{
    /**
     * @var ObserverInterface[]
     */
    private $observers;

    public function attach(ObserverInterface $observer)
    {
        $this->observers[]= $observer;
    }

    public function detach(ObserverInterface $observer)
    {
        foreach ($this->observers as $key => $value) {
            unset($this->observers[$key]);
        }
    }

    public function notify()
    {
        foreach ($this->observers as $item) {
            $item->update();
        }
    }


}