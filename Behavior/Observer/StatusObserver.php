<?php

namespace PHPPatterns\Behavior\Observer;

class StatusObserver implements ObserverInterface
{
    /**
     * @var bool
     */
    private $status;

    /**
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @void bool
     */
    public function update()
    {
        $this->status = !$this->status;
    }
}