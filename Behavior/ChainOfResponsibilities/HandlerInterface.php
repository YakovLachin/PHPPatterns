<?php

namespace PHPPatterns\Behavior\ChainOfResponsibilities;

interface HandlerInterface
{
    /**
     * @param Request $request
     */
    public function handle(Request $request);

    /**
     * @param Request $request
     * @return mixed
     */
    public function handleRequest(Request $request);

    /**
     * @param Request $request
     * @return bool
     */
    public function canHandleRequest(Request $request);
}