<?php

namespace PHPPatterns\Behavior\ChainOfResponsibilities;

interface Request
{
    /**
     * @return mixed
     */
    public function getBody();
}