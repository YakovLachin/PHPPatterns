<?php

namespace PHPPatterns\Behavior\ChainOfResponsibilities;

class BananaRequest implements Request
{
    /**
     * @return mixed
     */
    public function getBody()
    {
    }
}