<?php

namespace PHPPatterns\Behavior\ChainOfResponsibilities;

class AppleRequest implements Request
{
    /**
     * @return mixed
     */
    public function getBody()
    {
    }
}