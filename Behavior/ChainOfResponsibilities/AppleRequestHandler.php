<?php

namespace PHPPatterns\Behavior\ChainOfResponsibilities;

class AppleRequestHandler extends AbstractHandler
{

    /**
     * @param Request $request
     * @return mixed
     */
    public function handleRequest(Request $request)
    {
        return "Apple Response";
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function canHandleRequest(Request $request)
    {
        if (AppleRequest::class == get_class($request)) {
            return true;
        }

        return false;
    }
}