<?php

namespace PHPPatterns\Behavior\ChainOfResponsibilities;

abstract class AbstractHandler implements HandlerInterface
{
    /**
     * @var HandlerInterface
     */
    protected $next;

    /**
     * @param mixed $next
     */
    public function setNext($next)
    {
        $this->next = $next;
    }


    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function handle(Request $request)
    {
        if ($this->canHandleRequest($request)) {
            return $this->handleRequest($request);
        }

        if (isset($this->next)) {
            return $this->next->handle($request);
        }

        throw new \Exception();
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    abstract public function handleRequest(Request $request);

    /**
     * @param Request $request
     *
     * @return bool
     */
    abstract public function canHandleRequest(Request $request);
}