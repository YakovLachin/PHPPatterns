<?php

namespace PHPPatterns\Behavior\ChainOfResponsibilities;

class BananaRequestHandler extends AbstractHandler
{
    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function handleRequest(Request $request)
    {
        return "Banana Response";
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function canHandleRequest(Request $request)
    {
        if (BananaRequest::class == get_class($request)) {
            return true;
        }

        return false;
    }
}