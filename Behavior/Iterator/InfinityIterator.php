<?php

namespace PHPPatterns\Behavior\Iterator;

class InfinityIterator implements \Countable, \Iterator
{
    /**
     * @var int
     */
    protected $index;

    /**
     * @var array
     */
    protected $value;

    /**
     * @var int
     */
    protected $limit;

    /**
     * @var int
     */
    protected $counter;

    /**
     * InfinityIterator constructor.
     * @param array $value
     * @param int $limit
     */
    public function __construct(array $value, $limit)
    {
        $this->value = $value;
        $this->limit = $limit;
    }

    /**
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current()
    {
        $this->counter++;

        return $this->value[$this->index];
    }

    /**
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        if ($this->index > $this->count() - 2) {
            $this->rewind();
        } else {
            $this->index++;
        }
    }

    /**
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key()
    {
        return $this->index;
    }

    /**
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid()
    {
        return $this->counter < $this->limit;
    }

    /**
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        $this->index = 0;
    }

    /**
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     * @since 5.1.0
     */
    public function count()
    {
        return count($this->value);
    }
}