<?php

namespace PHPPatterns\GRASP\InformationExpert;

/**
 * Владеет информацией о количестве выбранных товаров
 */
class ProductLine
{
    /**
     * Спецификация продукта
     *
     * @var ProductSpecification
     */
    protected $product;

    /**
     * Кол-во выбранной позиции.
     *
     * @var int 
     */
    protected $quantity;

    /**
     * @param ProductSpecification $product  Спецификация родукта.
     * @param int                  $quantity Количество продукта.
     */
    public function __construct(ProductSpecification $product, $quantity)
    {
        $this->product  = $product;
        $this->quantity = $quantity;
    }

    /**
     * Возвращает значение промежуточной стоимости товара.
     *
     * @return float
     */
    public function getSubTotal()
    {
        return $this->quantity * $this->product;
    }
}