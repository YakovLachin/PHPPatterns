<?php

namespace PHPPatterns\GRASP\InformationExpert;

/**
 * Обладает общей информацией о всех товарах в чеке.
 */
class Sale
{
    /**
     * Список выбранных позици продуктов.
     *
     * @var []ProducLine
     */
    protected $productLines;

    /**
     * $param []ProductLine $productLines
     */
    public function __construct(array $productLines)
    {
        $this->productLiness = $productLines;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        $result = 0;
        foreach ($this->productLines as $line) {
            $subtotal = $line->getSubTotal();
            $result   = $result + $subtotal;
        }

        return $result;
    }
}