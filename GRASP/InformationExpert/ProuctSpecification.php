<?php
/**
 * Класс который должен владеть информаци о продукте.
 */

 namespace PHPPatterns\GRASP\InformationExpert;

/**
 * Владеет инормацией за еденицу товара
 */
class ProductSpecification
{
    /**
     * Стоимость Товара.
     *
     * @var float
     */
    protected $price;
    
    /**
     * @param float $price Cтоимость за еденицу продукции.
     */
    public function __construct($price)
    {
        $this->price = $price;
    }

    /**
     * Возвращает стоимость за один товар.
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }
}