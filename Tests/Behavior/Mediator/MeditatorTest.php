<?php

namespace PHPPatterns\Tests\Behavior\Mediator;

use PHPPatterns\Behavior\Mediator\Button;
use PHPPatterns\Behavior\Mediator\Input;
use PHPPatterns\Behavior\Mediator\Mediator;
use PHPPatterns\Behavior\Mediator\ResultOutput;
use PHPUnit\Framework\TestCase;

class MeditatorTest extends TestCase
{
    public function testMediator()
    {

        $mediator = new Mediator();
        $form     = new Input($mediator);
        $button   = new Button($mediator);
        $output   = new ResultOutput($mediator);

        $mediator->setFormInput($form);
        $mediator->setSubmitButton($button);
        $mediator->setOutput($output);

        $form->setStatement(true);
        $this->assertEquals("Input is 1", $output->getOutput());

        $button->onclick();
        $this->assertEquals("Form is submitted", $output->getOutput());
    }
}