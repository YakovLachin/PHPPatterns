<?php

namespace PHPPatterns\Tests\Behavior\Interpreter;


use PHPPatterns\Behavior\Interpreter\Context;
use PHPPatterns\Behavior\Interpreter\EqualExpression;
use PHPPatterns\Behavior\Interpreter\LiteralExpression;
use PHPPatterns\Behavior\Interpreter\VariableExpression;
use PHPUnit\Framework\TestCase;

class InterpreterTest extends TestCase
{
    public function testInterpreter_CorrectAST_CorrectInterpret()
    {
        $var = new VariableExpression("name", "Anna");
        $lit = new LiteralExpression("Anna");
        $bool = new EqualExpression($var, $lit);
        $context = New Context();
        $bool->interpret($context);
        $this->assertTrue($context->lookup($bool));
    }
}