<?php

namespace PHPPatterns\Tests\Behavior\Command;

use PHPPatterns\Behavior\Command;
use PHPPatterns\Behavior\Command\CoolComboCommand;
use PHPPatterns\Behavior\Command\Receiver;
use PHPPatterns\Behavior\Command\SimpleComboCommand;
use PHPUnit\Framework\TestCase;

class CommandTest extends TestCase
{
    public function testCommand_SimpleCombo_ExecutedIsCorrect()
    {
        $receiver = new Receiver();
        $abCommand = new SimpleComboCommand($receiver);
        $invoker = new Command\Invoker($abCommand);
        $actual = $invoker->run();

        $this->assertEquals("hook and lower kick", $actual);
    }
    public function testCommand_CoolCombo_ExecutedIsCorrect()
    {
        $receiver = new Receiver();
        $abCommand = new CoolComboCommand($receiver);
        $invoker = new Command\Invoker($abCommand);
        $actual = $invoker->run();

        $this->assertEquals("lower kick and hook and lower kick and lower kick and lower kick", $actual);
    }
}
