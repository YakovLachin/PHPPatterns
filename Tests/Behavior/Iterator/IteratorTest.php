<?php

namespace PHPPatterns\Tests\Behavior\Iterator;

use PHPUnit\Framework\TestCase;

class IteratorTest extends TestCase
{
    public function testIterator_limitIsSix_ReturnSixTimes()
    {
        $data = array("val1", "val2", "val3");
        $limit = 6;

        $infinityIterator = new \PHPPatterns\Behavior\Iterator\InfinityIterator($data, $limit);

        $actual = array();

        foreach ($infinityIterator as $value) {
            $actual[]= $value;
        }

        $expected = array(
            "val1",
            "val2",
            "val3",
            "val1",
            "val2",
            "val3",
        );

        $this->assertEquals($expected, $actual);
    }

    public function testReverseIterator_simplearray_ReverseISCorrect()
    {
        $data = array("val1", "val2", "val3");

        $infinityIterator = new \PHPPatterns\Behavior\Iterator\ReverseIterator($data);

        $actual = array();

        foreach ($infinityIterator as $value) {
            $actual[]= $value;
        }

        $expected = array(
            "val3",
            "val2",
            "val1",
        );

        $this->assertEquals($expected, $actual);
    }
}