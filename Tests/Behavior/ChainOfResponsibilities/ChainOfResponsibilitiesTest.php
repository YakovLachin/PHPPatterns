<?php

namespace PHPPatterns\Tests\Behavior\ChainOfResponsibilities;

use PHPPatterns\Behavior\ChainOfResponsibilities\AppleRequest;
use PHPPatterns\Behavior\ChainOfResponsibilities\AppleRequestHandler;
use PHPPatterns\Behavior\ChainOfResponsibilities\BananaRequest;
use PHPPatterns\Behavior\ChainOfResponsibilities\BananaRequestHandler;
use PHPPatterns\Behavior\ChainOfResponsibilities\CarrotRequest;
use PHPUnit\Framework\TestCase;

class ChainOfResponsibilitiesTest extends TestCase
{
    public function testHandle_AppleRequest_RequestIsHandle()
    {
        $appleHandler = new AppleRequestHandler();
        $bananaHandler = new BananaRequestHandler();
        $appleHandler->setNext($bananaHandler);
        $request = new AppleRequest();
        $actual = $appleHandler->handle($request);
        $this->assertEquals("Apple Response", $actual);
    }

    public function testHandle_BananaRequest_RequestIsHandle()
    {
        $appleHandler = new AppleRequestHandler();
        $bananaHandler = new BananaRequestHandler();
        $appleHandler->setNext($bananaHandler);
        $request = new BananaRequest();
        $actual = $appleHandler->handle($request);
        $this->assertEquals("Banana Response", $actual);
    }

    public function testHandle_CarrotRequest_ThrowException()
    {
        $appleHandler = new AppleRequestHandler();
        $bananaHandler = new BananaRequestHandler();
        $appleHandler->setNext($bananaHandler);
        $request = new CarrotRequest();
        $this->expectException(\Exception::class);
        $appleHandler->handle($request);
    }
}