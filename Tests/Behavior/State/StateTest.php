<?php

namespace PHPPatterns\Tests\Behavior\State;

use PHPPatterns\Behavior\State\Context;
use PHPPatterns\Behavior\State\IsNotActiveState;
use PHPPatterns\Behavior\State\ResultOutput;
use PHPUnit\Framework\TestCase;

class StateTest extends TestCase
{
    public function testState()
    {
        $context = new Context();
        $out     = new ResultOutput();
        $state   = new IsNotActiveState($out, $context);
        $context->setState($state);
        $context->operation();
        $this->assertEquals("Is active", $out->getOutput());
        $context->operation();
        $this->assertEquals("Is not active", $out->getOutput());
    }
}