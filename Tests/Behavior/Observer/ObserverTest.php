<?php

namespace PHPPatterns\Tests\Behavior\Observer;

use PHPPatterns\Behavior\Observer\CountObserver;
use PHPPatterns\Behavior\Observer\StatusObserver;
use PHPPatterns\Behavior\Observer\Subject;
use PHPUnit\Framework\TestCase;

class ObserverTest extends TestCase
{
    public function testObserver()
    {
        $subject     = new Subject();
        $countObsrv  = new CountObserver();
        $statusObsrv = new StatusObserver();
        $subject->attach($countObsrv);
        $subject->attach($statusObsrv);

        $subject->notify();
        $this->assertEquals(1, $countObsrv->getCount());
        $this->assertTrue($statusObsrv->getStatus());
    }
}