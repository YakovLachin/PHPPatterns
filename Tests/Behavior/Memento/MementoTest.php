<?php

namespace PHPPatterns\Tests\Behavior\Memento;

use PHPPatterns\Behavior\Memento\Originator;
use PHPUnit\Framework\TestCase;

class MementoTest extends TestCase
{
    public function test_Memento()
    {
        $originator = new Originator();
        $originator->increment();
        $this->assertEquals(1, $originator->getState());
        $memento = $originator->createMemento();
        $originator->increment();
        $originator->increment();
        $originator->increment();
        $originator->increment();
        $this->assertEquals(5, $originator->getState());
        $originator->setMemento($memento);
        $this->assertEquals(1, $originator->getState());
    }
}