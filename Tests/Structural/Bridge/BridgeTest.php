<?php

use PHPPatterns\Structural\Bridge\AlertWriter;
use PHPPatterns\Structural\Bridge\HtmlFormatter;
use PHPPatterns\Structural\Bridge\JsonFormatter;
use PHPPatterns\Structural\Bridge\MessageWriter;
use PHPUnit\Framework\TestCase;

class BridgeTest extends TestCase
{
    public function test_Bridge_MessageWriter_ReturnRequiredFormats()
    {
        $formatter     = new HtmlFormatter();
        $messageWriter = new MessageWriter($formatter);
        $htmlMessage   = $messageWriter->handle();
        $this->assertEquals("<h2>We need more minerals!!!</h2>", $htmlMessage);
        $messageWriter->setFormatter(new JsonFormatter());
        $jsonActual = $messageWriter->handle();
        $this->assertEquals('{"message":"We need more minerals!!!"}', $jsonActual);
    }

    public function test_Bridge_AlertWriter_ReturnRequiredFormats()
    {
        $formatter   = new HtmlFormatter();
        $alertWriter = new AlertWriter($formatter);
        $htmlAlert   = $alertWriter->handle();
        $this->assertEquals("<h2>We are under attack!!!</h2>", $htmlAlert);
        $alertWriter->setFormatter(new JsonFormatter());
        $jsonAlert = $alertWriter->handle();
        $this->assertEquals('{"message":"We are under attack!!!"}', $jsonAlert);
    }

}
