<?php

namespace PHPPatterns\Tests\Structural\Adapter;

use PHPPatterns\Structural\Adapter\Client;
use PHPPatterns\Structural\Adapter\IntCalculator;
use PHPPatterns\Structural\Adapter\IntCalculatorToExecutorAdapter;
use PHPPatterns\Structural\Adapter\Product;
use PHPUnit\Framework\TestCase;

class AdapterTest extends TestCase
{
    public function test_Adapter_UseCalculatorViaAdapter_ReturnCorrectResult()
    {
        $product = new Product();
        $product->setPrice(100);
        $alienCalculator    = new IntCalculator();
        $requiredCalculator = new IntCalculatorToExecutorAdapter($alienCalculator);
        $client = new Client($requiredCalculator);

        $actual = $client->getPrice($product);

        $this->assertEquals(200, $actual);
    }
}