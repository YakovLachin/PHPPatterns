<?php

namespace PHPPatterns\Tests\Structural\Composite;

use PHPPatterns\Structural\Composite\InputSubmitComponent;
use PHPPatterns\Structural\Composite\InputTextComponent;
use PHPPatterns\Structural\Composite\FormComposite;
use PHPUnit\Framework\TestCase;

class CompositeTest extends TestCase
{
    public function test_Composite_BuildFormComposite_ReturnForm()
    {
        $form = new FormComposite();
        $text = new InputTextComponent();
        $submit = new InputSubmitComponent();
        $form->add($text);
        $form->add($submit);
        $actual = $form->content();
        $expected = "<form><div><input type='text'></div><div><input type='submit'></div></form>";

        $this->assertEquals($expected, $actual);
    }

    public function test_Composite_BuildFormCompositeInFormComposite_ReturnForm()
    {
        $form   = new FormComposite();
        $text   = new InputTextComponent();
        $submit = new InputSubmitComponent();
        $form->add($text);
        $form->add($submit);

        $newForm = clone($form);
        $form->add($newForm);
        $form->remove($text);
        $form->remove($submit);
        $actual = $form->content();
        $expected = "<form><form><div><input type='text'></div><div><input type='submit'></div></form></form>";

        $this->assertEquals($expected, $actual);
    }
}