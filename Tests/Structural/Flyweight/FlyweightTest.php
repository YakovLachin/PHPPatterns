<?php

use PHPPatterns\Structural\Flyweight\ConcreteFlyweight;
use PHPPatterns\Structural\Flyweight\FlyweightFactory;
use PHPPatterns\Structural\Flyweight\UnsharedConcreteFlyweight;
use PHPUnit\Framework\TestCase;

class FlyweightTest extends TestCase
{
    public function test_GetFlyweight_CorrectKey_ReturnRequiredFlyweight()
    {
        $factory = new FlyweightFactory();
        $row = $factory->createRowFlyweight("Hello world!!");
        $res = $row->operation(" ");
        $this->assertEquals(" H e l l o   w o r l d ! !", $res);
        $this->assertEquals(9, $factory->count());
    }
}
