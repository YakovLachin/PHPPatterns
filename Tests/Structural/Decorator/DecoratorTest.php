<?php

namespace PHPPatterns\Tests\Structural;

use PHPPatterns\Structural\Decorator\BorderDecorator;
use PHPPatterns\Structural\Decorator\TextView;
use PHPPatterns\Structural\Decorator\TitleDecorator;
use PHPUnit\Framework\TestCase;

class DecoratorTest extends TestCase
{
    public function test_Decorator_UsedDecorators_DrawIsCorrect()
    {
        $view             = new TextView();
        $borderedView     = new BorderDecorator($view);
        $titleBorderedView = new TitleDecorator($borderedView);

        $expected = "<h2><div style='border: solid'><div>Text element</div></div></h2>";
        $actual   = $titleBorderedView->draw();
        $this->assertEquals($expected, $actual);
    }
}
