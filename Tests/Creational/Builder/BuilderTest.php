<?php

namespace PHPPatterns\Tests\Creational\Builder;

use PHPPatterns\Creational\Builder\Box;
use PHPPatterns\Creational\Builder\CheapBuilder;
use PHPPatterns\Creational\Builder\Director;
use PHPPatterns\Creational\Builder\ExpensiveBuilder;
use PHPUnit\Framework\TestCase;

class BuilderTest extends TestCase
{
     public function test_Builder_createProductViaCheapBuilder_ReturnCheapProduct()
    {
        $builder  = new CheapBuilder();
        $director = new Director($builder);
        $director->construct();

        $actual = $builder->getResult();
        $this->assertEquals(100, $actual->price);
        $this->assertEquals(10, $actual->markup);
        $this->assertEquals(Box::BAD, $actual->box->type);
    }
     public function test_Builder_createProductViaExpensiveBuilder_ReturnExpensiveProduct()
    {
        $builder  = new ExpensiveBuilder();
        $director = new Director($builder);
        $director->construct();

        $actual = $builder->getResult();

        $this->assertEquals(100, $actual->price);
        $this->assertEquals(200, $actual->markup);
        $this->assertEquals(Box::GOOD, $actual->box->type);
    }
}
