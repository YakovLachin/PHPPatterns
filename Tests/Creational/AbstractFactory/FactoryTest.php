<?php

namespace PHPPatterns\Tests\Creational\AbstractFactory;

use PHPPatterns\Creational\AbstractFactory\JSONFactory;
use PHPPatterns\Creational\AbstractFactory\Writer;
use PHPPatterns\Creational\AbstractFactory\XMLFactory;
use PHPUnit\Framework\TestCase;

class TestModel
{

    /**
     * @var string
     */
    public $name;
    /**
     * @var int
     */
    public $age;
    /**
     * @var string
     */
    public $email;

    /**
     * TestModel constructor.
     *
     * @param string $name
     * @param int $age
     * @param string $email
     */
    public function __construct($name, $age, $email)
    {
        $this->name = $name;
        $this->age = $age;
        $this->email = $email;
    }

}

class FactoryTest extends TestCase
{
    public function testAbstractFactory_UsedJSONFactory_ReturnJSONObject()
    {
        $model   = new TestModel("TestName", 32, "test@email.ru");
        $factory = new JSONFactory($model);
        $writer  = new Writer();


        $this->expectOutputString('{"name":"TestName","age":32,"email":"test@email.ru"}');
        $writer->write($factory->createObject());
    }
    public function testAbstractFactory_UsedXMLFactory_ReturnXMLObject()
    {
        $model   = new TestModel("TestName", 32, "test@email.ru");
        $factory = new XMLFactory($model);
        $writer  = new Writer();

        $this->expectOutputString('<?xml version="1.0"?>
<data><name>TestName</name><age>32</age><email>test@email.ru</email></data>
');
        $writer->write($factory->createObject());
    }
}
