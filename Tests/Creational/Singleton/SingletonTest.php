<?php

namespace PHPPatterns\Tests\Creational\Singleton;

use PHPPatterns\Creational\Singleton\Singleton;
use PHPUnit\Framework\TestCase;

class SingletonTest extends TestCase
{
    public function test_Singleton_InitInstance_InstanceIsSame()
    {
        $firstSingleton  = Singleton::getInstance();
        $secondSingleton = Singleton::getInstance();
        $this->assertSame($firstSingleton, $secondSingleton);
    }
}
