<?php

namespace PHPPatterns\Tests\Creational\Prototype;

use PHPPatterns\Creational\Prototype\Product;
use PHPPatterns\Creational\Prototype\PrototypeProductFactory;
use PHPUnit\Framework\TestCase;

class PrototypeTest extends TestCase
{
    function testPrototype_CloneProduct_ProductsIsNotSame()
    {
        $product = new Product();
        $product->setName("cap");
        $product->setPrice(120);
        $product->setScu("scunumbe");

        $actual = $product->cloneSelf();
        $this->assertNotSame($actual, $product);
    }

    function testPrototype_CloneProduct_ActualSCUIsEmpty()
    {
        $product = new Product();
        $product->setName("cap");
        $product->setPrice(120);
        $product->setScu("scunumber");

        $actual = $product->cloneSelf();
        $this->assertEmpty($actual->getScu());
    }


    function testPrototype_CloneProductViaFactory_ActualIsNotSame()
    {
        $product = new Product();
        $product->setName("cap");
        $product->setPrice(120);
        $product->setScu("scunumber");

        $prototypeFactory = new PrototypeProductFactory($product);
        $actual = $prototypeFactory->getProduct();
        $this->assertNotSame($actual, $product);
    }

    function testPrototype_CloneProductViaFactory_ActualSCUIsEmpty()
    {
        $product = new Product();
        $product->setName("cap");
        $product->setPrice(120);
        $product->setScu("scunumber");

        $prototypeFactory = new PrototypeProductFactory($product);
        $actual = $prototypeFactory->getProduct();
        $this->assertEmpty($actual->getScu());
    }
}
