<?php

namespace PHPPatterns\Tests\Creational\FactoryMethod;

use PHPPatterns\Creational\FactoryMethod\ExpensiveFactory;
use PHPPatterns\Creational\FactoryMethod\GuchiFactory;
use PHPPatterns\Creational\FactoryMethod\ProductFactory;
use PHPPatterns\Creational\FactoryMethod\SelfFactory;
use PHPUnit\Framework\TestCase;

class FactoryMethodTest extends TestCase
{
    public function testFactoryMethod_UsingFactory_ReturnProductMarkup()
    {
        $factory = new ProductFactory();
        $product = $factory->makeProduct();
        $product->setPrice(500);
        $this->assertEquals(750, $product->getTotal());
    }

    public function testFactoryMethod_UsedIsEccoFactory_ReturnEccoProductWithMarkup()
    {
        $factory = new ExpensiveFactory();
        $product = $factory->makeProduct();
        $product->setPrice(500);
        $this->assertEquals(1250, $product->getTotal());
    }
}
